<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<div class="row justify-content-md-center">

    <div class="login-box mx-auto">
        <div class="capital-heading text-center">Login</div>
        <?= $this->Flash->render() ?>

        <?= $this->Form->create() ?>
            <?= $this->Form->control('username', [
                'class' => 'col-12 input-background-child-md'
            ]) ?>
            <?= $this->Form->control('password', [
                'class' => 'col-12 input-background-child-md'
            ]) ?>
            <?= $this->Html->link('Forgot password?', [
                'controller' => 'Forgot',
                'action' => 'index',
                'password'
            ]) ?>
            <div class="userloginbuttoncenter">
                <?= $this->Form->submit('Submit', [
                    'class' => 'btn cdli-btn-blue'
                ]) ?>
            </div>
        <?= $this->Form->end() ?>
    </div>

</div>
