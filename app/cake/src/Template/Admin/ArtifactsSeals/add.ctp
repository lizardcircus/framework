<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsSeal $artifactsSeal
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsSeal) ?>
            <legend class="capital-heading"><?= __('Add Artifacts Seal') ?></legend>
            <?php
                echo $this->Form->control('seal_no');
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Artifacts Seals'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
