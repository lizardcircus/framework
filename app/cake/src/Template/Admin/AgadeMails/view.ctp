<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AgadeMail $agademail
 */
?>

<p align="center"> 
<?= $this->Html->link('Previous', ['action' => ''.$prev_id], ['class' => 'btn btn-primary '.(($prev_id == null) ? 'disabled':'')]) ?>
<?= $this->Html->link('Next', ['action' => ''.$next_id], ['class' => 'btn btn-primary '.(($next_id == null) ? 'disabled':'')]) ?>
</p>
<div class="row justify-content-md-center">

    <div class="boxed">
        <div class="capital-heading"><?= __('View Email') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Title') ?></th>
                    <td><?= h($agademail->title) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Date') ?></th>
                    <td><?= h($agademail->date) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Category') ?></th>
                    <td><?= h($agademail->category) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('CDLI Tag') ?></th>
                    <td><?= h($agademail->cdli_tag) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Content') ?></th>
                    <!-- Formatting for retaining new line characters and displaying clickable links and emails -->
                    <td>
                        <?php $string = nl2br( str_replace('>',' ', str_replace('<',' ',$agademail->content)) );
                        $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i'; 
                        $string = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $string);
                        $search  = array('/<p>__<\/p>/', '/([a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/');
                        $replace = array('<hr />', '<a href="mailto:$1">$1</a>');
                        $processed_string = preg_replace($search, $replace, $string);
                        echo $processed_string; ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <p align="center"><?= $this->Html->link('Go back', ['action' => 'index'], ['class' => 'btn btn-primary']) ?></p>
</div>
