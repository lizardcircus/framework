<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsPublication $artifactsPublication
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsPublication, ['action' => 'edit/'.$artifactsPublication->id.'/'.$flag.'/'.$parent_id]) ?>
        <legend class="capital-heading"><?= __('Edit Artifacts Publication') ?></legend>
        <table cellpadding="10" cellspacing="10">
            <tr>
                <td> Publication ID: <td>
                <td><?php  echo $this->Form->control('publication_id', ['label' => '', 'type' => 'number', 'disabled' => 'disabled']);
                echo $this->Form->control('publication_id', ['type' => 'hidden']); ?><td>
            </tr>
            <tr>
                <td> Artifact ID: <td>
                <td><?php echo $this->Form->control('artifact_id', ['label' => '', 'type' => 'number', 'disabled' => 'disabled']);
                echo $this->Form->control('artifact_id', ['type' => 'hidden']); ?><td>
            </tr>
            <tr>
                <td> Exact Reference:  <td>
                <td><?php echo $this->Form->control('exact_reference', ['label' => '', 'type' => 'text', 'required' => false]); ?><td>
            </tr>
            <tr>
                <td> Publication Type: <td>
                <td><?php $options = [
                    'primary' => 'primary',
                    'electronic' => 'electronic',
                    'citation' => 'citation',
                    'collation' => 'collation',
                    'history' => 'history',
                    'other' => 'other'
                ];
                echo $this->Form->control('publication_type', ['label' => '', 'type' => 'select', 'options' => $options, 'value' => $artifactsPublication->publication_type] ); ?><td>
            </tr>
            <tr>
                <td> Publication Comments: <td>
                <td><?php echo $this->Form->control('publication_comments', ['label' => '', 'type' => 'text']); ?><td>
            </tr>
            <tr>
                <td> Artifact Designation: <td>
                <td><?php echo $this->Form->control('artifact_designation', ['label' => '', 'type' => 'text', 'disabled' => 'disabled', 'value' => $artifactsPublication->artifact->designation]); ?><td>
            </tr>
            <tr>
                <td> Publication Designation: <td>
                <td><?php echo $this->Form->control('publication_designation', ['label' => '', 'type' => 'text', 'disabled' => 'disabled', 'value' => $artifactsPublication->publication->designation ]); ?><td>
            </tr>
            <tr>
                <td> Publication Reference: <td>
                <td> To be added <td>
            </tr>
        </table>
        <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List All Artifacts Publications Links'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
