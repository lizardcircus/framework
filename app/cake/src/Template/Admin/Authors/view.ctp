<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author $author
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Author') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Full Name') ?></th>
                    <td><?= h($author->author) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Author Id') ?></th>
                    <td><?= $this->Number->format($author->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('First Name') ?></th>
                    <td><?= h($author->first) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Last Name') ?></th>
                    <td><?= h($author->last) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Institution') ?></th>
                    <td><?= h($author->institution) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Email') ?></th>
                    <td><?= h($author->email) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Last Modified') ?></th>
                    <td><?= h($author->email) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('ORCID ID') ?></th>
                    <td><?= h($author->orcid_id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Author'), ['action' => 'edit', $author->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Author'), ['action' => 'delete', $author->id], ['confirm' => __('Are you sure you want to delete # {0}?', $author->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Authors'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Cdl Notes'), ['controller' => 'CdlNotes', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Cdl Note'), ['controller' => 'CdlNotes', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Credits'), ['controller' => 'Credits', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Credit'), ['controller' => 'Credits', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>


<div class="boxed mx-0">
    <?php if (empty($author->publications)): ?>
        <!-- <div class="capital-heading"><?= __('No Related Publications') ?></div> -->
        <div class="capital-heading"><?= __('Related Publications: To be added') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Publications') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Bibtexkey') ?></th>
                <th scope="col"><?= __('Year') ?></th>
                <th scope="col"><?= __('Entry Type Id') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Annote') ?></th>
                <th scope="col"><?= __('Book Title') ?></th>
                <th scope="col"><?= __('Chapter') ?></th>
                <th scope="col"><?= __('Crossref') ?></th>
                <th scope="col"><?= __('Edition') ?></th>
                <th scope="col"><?= __('Editor') ?></th>
                <th scope="col"><?= __('How Published') ?></th>
                <th scope="col"><?= __('Institution') ?></th>
                <th scope="col"><?= __('Journal Id') ?></th>
                <th scope="col"><?= __('Month') ?></th>
                <th scope="col"><?= __('Note') ?></th>
                <th scope="col"><?= __('Number') ?></th>
                <th scope="col"><?= __('Organization') ?></th>
                <th scope="col"><?= __('Pages') ?></th>
                <th scope="col"><?= __('Publisher') ?></th>
                <th scope="col"><?= __('School') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Volume') ?></th>
                <th scope="col"><?= __('Publication History') ?></th>
                <th scope="col"><?= __('Abbreviation Id') ?></th>
                <th scope="col"><?= __('Series') ?></th>
                <th scope="col"><?= __('Oclc') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </thead>
            <tbody>
                <?php foreach ($author->publications as $publications): ?>
                <tr>
                    <td><?= h($publications->id) ?></td>
                    <td><?= h($publications->bibtexkey) ?></td>
                    <td><?= h($publications->year) ?></td>
                    <td><?= h($publications->entry_type_id) ?></td>
                    <td><?= h($publications->address) ?></td>
                    <td><?= h($publications->annote) ?></td>
                    <td><?= h($publications->book_title) ?></td>
                    <td><?= h($publications->chapter) ?></td>
                    <td><?= h($publications->crossref) ?></td>
                    <td><?= h($publications->edition) ?></td>
                    <td><?= h($publications->editor) ?></td>
                    <td><?= h($publications->how_published) ?></td>
                    <td><?= h($publications->institution) ?></td>
                    <td><?= h($publications->journal_id) ?></td>
                    <td><?= h($publications->month) ?></td>
                    <td><?= h($publications->note) ?></td>
                    <td><?= h($publications->number) ?></td>
                    <td><?= h($publications->organization) ?></td>
                    <td><?= h($publications->pages) ?></td>
                    <td><?= h($publications->publisher) ?></td>
                    <td><?= h($publications->school) ?></td>
                    <td><?= h($publications->title) ?></td>
                    <td><?= h($publications->volume) ?></td>
                    <td><?= h($publications->publication_history) ?></td>
                    <td><?= h($publications->abbreviation_id) ?></td>
                    <td><?= h($publications->series) ?></td>
                    <td><?= h($publications->oclc) ?></td>
                    <td><?= h($publications->designation) ?></td>
                    <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Publications', 'action' => 'view', $publications->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Publications', 'action' => 'edit', $publications->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Publications', 'action' => 'delete', $publications->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $publications->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<div class="boxed mx-0">
    <?php if (empty($author->cdl_notes)): ?>
        <!-- <div class="capital-heading"><?= __('No Related Cdl Notes') ?></div> -->
        <div class="capital-heading"><?= __('Related Cdl Notes: To be added') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Cdl Notes') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Year') ?></th>
                <th scope="col"><?= __('Number') ?></th>
                <th scope="col"><?= __('Author Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Text') ?></th>
                <th scope="col"><?= __('Footnotes') ?></th>
                <th scope="col"><?= __('Bibliography') ?></th>
                <th scope="col"><?= __('Uploaded') ?></th>
                <th scope="col"><?= __('Preprint') ?></th>
                <th scope="col"><?= __('Archival') ?></th>
                <th scope="col"><?= __('Preprint State') ?></th>
                <th scope="col"><?= __('Online') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </thead>
            <tbody>
                <?php foreach ($author->cdl_notes as $cdlNotes): ?>
                <tr>
                    <td><?= h($cdlNotes->id) ?></td>
                    <td><?= h($cdlNotes->year) ?></td>
                    <td><?= h($cdlNotes->number) ?></td>
                    <td><?= h($cdlNotes->author_id) ?></td>
                    <td><?= h($cdlNotes->title) ?></td>
                    <td><?= h($cdlNotes->text) ?></td>
                    <td><?= h($cdlNotes->footnotes) ?></td>
                    <td><?= h($cdlNotes->bibliography) ?></td>
                    <td><?= h($cdlNotes->uploaded) ?></td>
                    <td><?= h($cdlNotes->preprint) ?></td>
                    <td><?= h($cdlNotes->archival) ?></td>
                    <td><?= h($cdlNotes->preprint_state) ?></td>
                    <td><?= h($cdlNotes->online) ?></td>
                    <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'CdlNotes', 'action' => 'view', $cdlNotes->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'CdlNotes', 'action' => 'edit', $cdlNotes->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'CdlNotes', 'action' => 'delete', $cdlNotes->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $cdlNotes->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<div class="boxed mx-0">
    <?php if (empty($author->credits)): ?>
        <!-- <div class="capital-heading"><?= __('No Related Credits') ?></div> -->
        <div class="capital-heading"><?= __('Related Credits: To be added') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Credits') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Author Id') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Comments') ?></th>
                <th scope="col"><?= __('Credit To') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </thead>
            <tbody>
                <?php foreach ($author->credits as $credits): ?>
                <tr>
                    <td><?= h($credits->id) ?></td>
                    <td><?= h($credits->author_id) ?></td>
                    <td><?= h($credits->artifact_id) ?></td>
                    <td><?= h($credits->date) ?></td>
                    <td><?= h($credits->comments) ?></td>
                    <td><?= h($credits->credit_to) ?></td>
                    <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Credits', 'action' => 'view', $credits->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Credits', 'action' => 'edit', $credits->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Credits', 'action' => 'delete', $credits->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $credits->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<div class="boxed mx-0">
    <?php if (empty($author->users)): ?>
        <!-- <div class="capital-heading"><?= __('No Related Users') ?></div> -->
        <div class="capital-heading"><?= __('Related Users: To be added') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Users') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Username') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Created By') ?></th>
                <th scope="col"><?= __('Admin') ?></th>
                <th scope="col"><?= __('Download Hd Images') ?></th>
                <th scope="col"><?= __('Filtering') ?></th>
                <th scope="col"><?= __('Can Download Hd Images') ?></th>
                <th scope="col"><?= __('Can View Private Catalogues') ?></th>
                <th scope="col"><?= __('Can View Private Transliterations') ?></th>
                <th scope="col"><?= __('Can Edit Transliterations') ?></th>
                <th scope="col"><?= __('Can View Private Images') ?></th>
                <th scope="col"><?= __('Can View IPadWeb') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Author Id') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </thead>
            <tbody>
                <?php foreach ($author->users as $users): ?>
                <tr>
                    <td><?= h($users->id) ?></td>
                    <td><?= h($users->username) ?></td>
                    <td><?= h($users->created) ?></td>
                    <td><?= h($users->created_by) ?></td>
                    <td><?= h($users->admin) ?></td>
                    <td><?= h($users->download_hd_images) ?></td>
                    <td><?= h($users->filtering) ?></td>
                    <td><?= h($users->can_download_hd_images) ?></td>
                    <td><?= h($users->can_view_private_catalogues) ?></td>
                    <td><?= h($users->can_view_private_transliterations) ?></td>
                    <td><?= h($users->can_edit_transliterations) ?></td>
                    <td><?= h($users->can_view_private_images) ?></td>
                    <td><?= h($users->can_view_iPadWeb) ?></td>
                    <td><?= h($users->email) ?></td>
                    <td><?= h($users->author_id) ?></td>
                    <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Users', 'action' => 'view', $users->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Users', 'action' => 'edit', $users->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Users', 'action' => 'delete', $users->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $users->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>


