<?php
/**
* CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
* @link          https://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       https://opensource.org/licenses/mit-license.php MIT License
*/


?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= $this->fetch('title') ?> - Cuneiform Digital Library Initiative
	</title>
	<?= $this->Html->meta('icon') ?>
	<?= $this->Html->css('main.css')?>
	<?= $this->Html->css('font-awesome/css/font-awesome.min.css')?>

	<?php
    // For using Bootstrap dropdowns, set variable $includePopper in your view
    if (isset($includePopper) && $includePopper) {
        echo $this->Html->script('popper.min.js');
    }
    ?>

	<?= $this->Html->script('jquery.min.js')?>
	<?= $this->Html->script('bootstrap.min.js')?>
	<?= $this->Html->script('js.js')?>


	<?= $this->Html->script('drawer.js', ['defer' => true]) ?>


    <?= $this->element('google-analytics'); ?>

	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>
</head>

<body>
	<div class="translucent-film d-none" id="faded-bg"></div>
	<noscript>
		<nav class="navbar navbar-expand-sm bg-light no-js-nav d-lg-none">
			<!-- Links -->
			<ul class="navbar-nav flex-row justify-content-around w-100">
				<li class="nav-item">
				<a class="nav-link mx-2" href="/browse">Browse</a>
				</li>
				<li class="nav-item">
				<a class="nav-link mx-2" href="/search">Search</a>
				</li>
				<li class="nav-item">
				<a class="nav-link mx-2" href="#">About</a>
				</li>
				<?php if (!$this->Session->read('Auth.User')) { ?>
					<li class="nav-item mx-2">
						<?= $this->Html->link("Login", ['controller' => 'users'], ['class' => 'nav-link']) ?>
					</li>
					<li class="nav-item mx-2">
						<?= $this->Html->link("Register", ['controller' => 'register'], ['class' => 'nav-link']) ?>
					</li>
				<?php } else { ?>
					<li class="nav-item mx-2">
						<?= $this->Html->link("Profile", ['controller' => 'users'], ['class' => 'nav-link']) ?>
					</li>
					<li class="nav-item mx-2">
						<?= $this->Html->link("Logout", ['controller' => 'logout'], ['class' => 'nav-link']) ?>
					</li>
				<?php } ?>
			</ul>

			<style>
			.fa-bars{
				display: none !important;
			}
			</style>
		</nav>
	</noscript>
	<nav class="navbar navbar-expand d-none d-lg-block">
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item mx-3">
					<a class="nav-link" href="#">Publications <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item mx-3">
					<a class="nav-link" href="#">Resources</a>
				</li>
				<?php if (!$this->Session->read('Auth.User')) { ?>
					<li class="nav-item mx-3">
						<?= $this->Html->link("Login", ['controller' => 'users'], ['class' => 'nav-link']) ?>
					</li>
					<li class="nav-item mx-3 pr-5">
						<?= $this->Html->link("Register", ['controller' => 'register'], ['class' => 'nav-link']) ?>
					</li>
				<?php } else { ?>
					<li class="nav-item mx-3">
						<?= $this->Html->link("Profile", [
                                'controller' => 'users',
                                'action' => 'view',
                                $this->Session->read('Auth.User.username')
                            ], [
                                'class' => 'nav-link'
                        ]) ?>
					</li>
					<li class="nav-item mx-3 pr-5">
						<?= $this->Html->link("Logout", ['controller' => 'logout'], ['class' => 'nav-link']) ?>
					</li>
				<?php } ?>
			</ul>
		</div>
	</nav>
	<nav class="header navbar navbar-expand-lg navbar-light bg-transparent py-0" id="navbar-main">
		<div class="container-fluid d-flex align-items-center justify-content-between my-5">
			<a class="navbar-brand logo" href="/">
				<div class="navbar-logo">
					<img src="/images/logo.png" class="d-none d-md-block" alt="cdli-logo" />
					<img src="/images/logo-no-text.svg" class="d-md-none" alt="cdli-logo" />
				</div>
			</a>

			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item mx-3 active">
						<!-- Check If user has admin access -->
						<?php if (!is_null($this->Session->read('Auth.User')) && in_array(1, $this->Session->read('Auth.User.roles'))) { ?>
							<?= $this->Html->link("Dashboard", ['controller' => 'Artifacts', 'action' => 'dashboard', 'prefix' => 'admin'], ['class' => 'nav-link']) ?>

						<?php } else { ?>
							<?= $this->Html->link("Browse", ['controller' => 'Artifacts', 'action' => 'browse'], ['class' => 'nav-link']) ?>							
						<?php } ?>
					</li>
					<li class="nav-item mx-3">
						<a class="nav-link" href="https://gitlab.com/cdli/framework" target="_blank">Contribute</a>
					</li>
					<li class="nav-item mx-3">
						<a class="nav-link" href="#">About</a>
					</li>
					<li class="nav-item mx-3 mr-5 dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search </a>
						<div class="dropdown-menu border-0 shadow" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="/AdvancedSearch">Advanced search</a>
							<a class="dropdown-item" href="#">Search settings</a>
						</div>
					</li>
				</ul>
			</div>
			<span 
			class="d-md-block d-sm-block d-lg-none d-xl-none fa fa-bars fa-2x" 
			onclick="openSlideMenu()" 
			aria-hidden="true">
			</span>
			
			<div id="side-menu" class="side-nav shadow">
				<p class="sidebar-btn-close px-3" onclick="closeSlideMenu()">×</p>
				<a href="/browse" class="mt-5">Browse</a>
				<a href="https://gitlab.com/cdli/framework" target="_blank">Contribute</a>
				<a href="#">About</a>
				<a onclick="toggleRotateArrow()" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="border-0 mb-1">
					Search <span id="drawer-arrow" class="fa fa-angle-down" aria-hidden="true"></span>
				</a>
				<div class="collapse embedded-links" id="collapseExample">
					<a href="/AdvancedSearch" class="ml-5">Advanced search</a>
					<a href="#" class="ml-5">Search settings</a>
				</div>
				<?php if (!$this->Session->read('Auth.User')):  ?>
					<?= $this->Html->link("Login", ['controller' => 'users'], ['class' => 'pt-4 login-border']) ?>
					<?= $this->Html->link("Register", ['controller' => 'register']) ?>
				<?php else: ?>
					<?= $this->Html->link("Profile", [
                        'controller' => 'users',
                        'action' => 'view',
                        $this->Session->read('Auth.User.username')
                        ], [
                            'class' => 'pt-4 login-border'
                        ]) ?>
					<?= $this->Html->link("Logout", ['controller' => 'logout']) ?>
				<?php endif; ?>

				<a href="#">Resources</a>
				<a href="#">Publications</a>
			</div>
			
		</div>
	</nav>

	<div class="container-fluid text-center contentWrapper">
		<?= $this->Flash->render() ?>
		<?= $this->fetch('content') ?>
	</div>

	<noscript>
		<p class="alert alert-danger alert-dismissible fade show textcenter">Your browser does not support javascript please enable it for better experince!</p>
	</noscript>
	

	<footer>
		<div class="container">
			<div>
				<div class="row footer-1 py-5">
					<div class="col-lg-3 d-none d-lg-block">
						<h2 class="heading">Navigate</h2>
						<p><a href="/browse">Browse collection</a></p>
						<p><a href="#">Contribute</a></p>
						<p><a href="#">About CDLI</a></p>
						<p><a href="#">Search collection</a></p>
					</div>
					<div class="col-md-6 col-lg-4">
						<h2 class="heading">Acknowledgement</h2>
						<p class="backers">
                            Support for this initiative has been generously provided by the 
							<a href="https://mellon.org/" target="_blank">Mellon Foundation</a>, 
                            the <a href="https://www.nsf.gov/" target="_blank">NSF</a>, 
							the <a href="https://www.neh.gov/" target="_blank">NEH</a>, 
							the <a href="https://www.imls.gov/" target="_blank">IMLS</a>, 
							the <a href="https://www.mpg.de/en" target="_blank">MPS</a>, 
							<a href="http://www.ox.ac.uk/" target="_blank">Oxford University</a> 
							and <a href="http://www.ucla.edu/" target="_blank">UCLA</a>, 
							with additional support
                            from <a href="https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx" target="_blank">SSHRC</a> and 
							the <a href="https://www.dfg.de/" target="_blank">DFG</a>. 
							Computational resources and network services are provided by 
							<a href="https://humtech.ucla.edu/" target="_blank">UCLA’s HumTech</a>,
                            <a href="https://www.mpiwg-berlin.mpg.de/" target="_blank">MPIWG</a>, 
							and <a href="https://www.computecanada.ca/" target="_blank">Compute Canada</a>.
                        </p>
					</div>
                    <div class="col-lg-1 d-none d-lg-flex"></div>
					<div class="col-md-6 col-lg-4 contact">
						<h2 class="heading">Contact Us</h2>
						<p class="p">Department of Near Eastern Languages and Cultures
							<br>378 Humanities Building, 415 Portola Plaza,<br>Los Angeles, CA 90095-1511, USA
						</p>
						<div class="d-flex">
							<div class="twitter">
								<a href="https://twitter.com/cdli_news" target="_blank" aria-label="twitter-page">
									<span class="fa fa-twitter" aria-hidden="true"></span>
								</a>
							</div>
							<div class="mail">
								<a href="mailto:cdli@ucla.edu" target="_blank" aria-label="mail-to-cdli">
									<span class="fa fa-envelope fa-3x" aria-hidden="true"></span>
								</a>
							</div>
							<div> <a href="https://giving.ucla.edu/Standard/NetDonate.aspx?SiteNum=245" class="btn donate" role="button" target="_blank">Donate</a></div>
						</div>
						<!-- <a
						href="https://www.w3.org/WAI/WCAG2AAA-Conformance"
						title="Explanation of WCAG 2.0 Level Triple-A Conformance">
						<img
						height="32"
						width="88"
						src="https://www.w3.org/WAI/wcag2AAA-blue"
						alt="Level Triple-A conformance, W3C WAI Web Content Accessibility Guidelines 2.0"
						/> -->
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="footer-end p-4">
	<div class="text-center text-white">
		© 2019-2020 Cuneiform Digital Library Initiative.
	</div>
</div>
</footer>
</body>
</html>
