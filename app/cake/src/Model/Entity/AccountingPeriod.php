<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AccountingPeriod Entity
 *
 * @property int $id
 * @property int $start_ruler_id
 * @property int $start_month_id
 * @property int $start_year_id
 * @property string $start_date
 * @property int $end_ruler_id
 * @property int $end_month_id
 * @property int $end_year_id
 * @property string $end_date
 *
 * @property \App\Model\Entity\Ruler $ruler
 * @property \App\Model\Entity\Month $month
 * @property \App\Model\Entity\Year $year
 */
class AccountingPeriod extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'start_ruler_id' => true,
        'start_month_id' => true,
        'start_year_id' => true,
        'start_date' => true,
        'end_ruler_id' => true,
        'end_month_id' => true,
        'end_year_id' => true,
        'end_date' => true,
        'ruler' => true,
        'month' => true,
        'year' => true
    ];
}
