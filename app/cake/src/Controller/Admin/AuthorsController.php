<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Authors Controller
 *
 * @property \App\Model\Table\AuthorsTable $Authors
 *
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AuthorsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 10,
            'order' => [
                'first' => 'ASC',
                'last' => 'ASC'
            ]
        ];

        $authors = $this->paginate($this->Authors);

        $this->set(compact('authors'));
    }

    /**
     * View method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $author = $this->Authors->get($id, [
            // 'contain' => ['Publications', 'CdlNotes', 'Credits', 'Users']
        ]);

        $this->set(compact('author'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $author = $this->Authors->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            foreach ($data as $key => $value) {
                $data[$key] = trim($value);
            }
            $data['first'] = ucfirst($data['first']);
            $data['last'] = ucfirst($data['last']);
            if ($data['first'] == '') {
                $data['author'] = $data['last'];
            } elseif ($data['last'] == '') {
                $data['author'] = $data['first'];
            } else {
                if ($data['east_asian_order']) {
                    $data['author'] = $data['last'].', '.$data['first'];
                } else {
                    $data['author'] = $data['first'].', '.$data['last'];
                }
            }
            $author = $this->Authors->patchEntity($author, $data);
            if ($this->Authors->save($author)) {
                $this->Flash->success(__('The author has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The author could not be saved. Please, try again.'));
        }
        $this->set(compact('author'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $author = $this->Authors->get($id);
        

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            foreach ($data as $key => $value) {
                $data[$key] = trim($value);
            }
            $data['first'] = ucfirst($data['first']);
            $data['last'] = ucfirst($data['last']);
            if ($data['first'] == '') {
                $data['author'] = $data['last'];
            } elseif ($data['last'] == '') {
                $data['author'] = $data['first'];
            } else {
                if ($data['east_asian_order']) {
                    $data['author'] = $data['last'].', '.$data['first'];
                } else {
                    $data['author'] = $data['first'].', '.$data['last'];
                }
            }
            debug($data);
            $author = $this->Authors->patchEntity($author, $data);
            debug($author);
            $author = $this->Authors->patchEntity($author, $this->request->getData());
            if ($this->Authors->save($author)) {
                $this->Flash->success(__('The author details have been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The author details could not be updated. Please, try again.'));
        }
        $this->set(compact('author'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $author = $this->Authors->get($id);
        if ($this->Authors->delete($author)) {
            $this->Flash->success(__('The author has been deleted.'));
        } else {
            $this->Flash->error(__('The author could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
