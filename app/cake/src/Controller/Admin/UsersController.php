<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function beforefilter(\Cake\Event\Event $event)
    {
        // Check if Authenticated User
        if (!is_null($this->Auth->user())) {
            // Check if Admin
            if (in_array(1, $this->Auth->user('roles'))) {
                $this->Auth->allow();
            }
        } else {
            $this->Auth->deny();
        }
    }

    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($username = null)
    {
        $user = $this->Users->findByUsername($username)->first();

        if ($user) {
            $modifiedUser = $this->Auth->user();
            $modifiedUser['username'] = $user['username'];
            $modifiedUser['email'] = $user['email'];
            $modifiedUser['last_login_at'] = $user['last_login_at'];
            $modifiedUser['active'] = $user['active'];
            $modifiedUser['modified_at'] = $user['modified_at'];
            $modifiedUser['created_at'] = $user['created_at'];
            $modifiedUser['roles'] = (new \App\Controller\TwofactorController())->getUsersRole($user['id']);

            $this->set('user', $modifiedUser);
        } else {
            $this->Flash->error("No such user exists");
            return $this->redirect('/');
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($username = null)
    {
        if ($this->Auth->user('username')) {
            $user = $this->Users->findByUsername($username)->first();
            $roles = (new \App\Controller\TwofactorController())->getUsersRole($user['id']);
            
            // Deleting Normal User Role ID
            if (($key = array_search(10, $roles)) !== false) {
                unset($roles[$key]);
            }
            $setUserDataForView = [];
            $setUserDataForView["username"] = $user["username"];
            $setUserDataForView["email"] = $user["email"];
            $setUserDataForView["created_at"] = $user["created_at"];
            $setUserDataForView["active"] = $user["active"];
            $setUserDataForView["roles"] = $roles;
        } else {
            $this->Flash->error(__('Access Denied.'));
            return $this->redirect('/');
        }

        if ($this->request->is(['post'])) {
            $formData = $this->request->getData();

            $activeStatus = array_key_exists('active', $formData) ? 1 : 0;

            if ($activeStatus) {
                unset($formData["active"]);
            }
            
            // If active status is toggled
            if ($activeStatus != $user["active"]) {
                if ($this->Auth->user('id') != $user['id']) {
                    $updateUser = $this->Users->patchEntity($user, ["active" => $activeStatus]);

                    if ($this->Users->save($updateUser)) {
                        $this->Flash->success(__("Active Status updated successfully."));
                    } else {
                        $this->Flash->error(__("Active Status cannot be updated."));
                    }
                } else {
                    $this->Flash->error(__("You cannot deactivate currently logged in account."));
                }
            }

            $updatedRoles = array_keys($formData);

            // New Roles added
            $toBeSet = array_diff($updatedRoles, $roles);
            $toBeSet = empty($toBeSet) ? null : $toBeSet;
            
            // Existing Roles Deleted
            $toBeRemoved = array_diff($roles, $updatedRoles);
            $toBeRemoved = empty($toBeRemoved) ? null : $toBeRemoved;

            if (!is_null($toBeSet) || !is_null($toBeRemoved)) {
                // Setting the new roles
                $roles = (new \App\Controller\TwofactorController())->setUsersRole($user['id'], $toBeSet, $toBeRemoved);
                
                // If users changes the role of currently loggedin account
                if ($this->Auth->user('id') == $user->id) {
                    $updateAuth = $this->Auth->user();
                    $updateAuth["roles"] = $roles;
                    $this->Auth->setUser($updateAuth);
                }

                $this->Flash->success(__("Roles Updated."));
            }
            $this->redirect(['action' => 'view', $user->username]);
        }
        
        $this->set('user', $setUserDataForView);
    }
}
